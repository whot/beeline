# beeline

beeline is a generic keyboard shortcut/macro handling approach.

**This project is for the discussion and collection of requirements.**

Do not expect it to work yet. To contribute, file an MR to expand this
README or file an issue for discussion.

## Summary

This projects aims to provide a keyboard macro handling implementation. A
keyboard macro is a sequence of key press/release events (including timing
information) that is triggered by a key event.

For example, a user may press Shift+F7 to trigger a macro that types "Hello
World!".

## Terminology

- beeline: this implementation. May be a library, may be a daemon, unclear
  at this point.
- Macro: a sequence of key events to be replayed by beeline.
- Profile: a set of macros currently configured for a device.
- Trigger: a key combination that causes the replay of a macro

## Requirements

beeline should be able to replay a macro with an arbitrary number of
characters in response to a trigger. Reasonable limits on keys and durations
may be applied.

The trigger may be any modifier + single key combination, multi-key triggers
are out of scope.

macros must be user-specific and may be application specific, i.e. "blender"
macro should only be active if the focused application is Blender.

## Components

The components required are:

- a method to store macros in persistent storage and load them
- a method to record macros, edit them and assign them to profiles
- a method/user interface to switch between profiles
- a method to identify the currently active application and switch profiles
- a method to identify triggers
- a method to replay the macro

The primary focus is a Wayland-enabled desktop. X support may be
coincidental.
